# Example pas à pas de création de service Ros

## 1- Package et descripteur de service

- Créer un package ROS comme suit

```
catkin_create_pkg img_producer std_msgs sensor_msgs rospy
```
- Lors de la création du package nous spécifions à ROS les dépendances dont nous aurons besoin

- Créer dans votre package un répertoire ```srv```. Ce package va contenir le descripteur, le contrat, de notre service

```
cd img_producer
mkdir srv
```
- Dans le répertoire ```srv``` créer le fichier suivant ```GetImg.srv``` comme suit:

```
string[] theme_list
int16 h
int16 w
---
sensor_msgs/Image img
```
- Explications:
    - le descripteur de service va définir le contrat associé au service que nous allons créer à savoir
        - les arguments d'entrée (zone au dessus des ```---``` )
        - les valeurs de retour (zone au dessous des ```---``` ) 
        - les arguments et valeurs ont le format ```<type> <nom>```. E.g ```"int16 h"```, ```int16``` est le type entier 16b et ```h``` est le nom de l'argument. Différents types par defaut existent (cf. http://wiki.ros.org/msg), mais vous pouvez également utiliser des types de message d'autres packages (e.g ```sensor_msgs/Image```) ou définir vos propres types (cf. http://wiki.ros.org/ROS/Tutorials/CreatingMsgAndSrv)

- Ajouter les lignes suivantes au fichier ```package.xml``` de votre package ```img_producer```

```xml
...
<build_depend>message_generation</build_depend>
<exec_depend>message_runtime</exec_depend>
...

```
- Explication:
    - Ces lignes de commandes vont permettre lors de la compilation du package de générer les descripteurs de messages ou services inclus dans le package courant. Ces derniers une fois compilés seront utilisables au sein d'autres packages.

- Modification du ```CMakeLists.txt```

    - Modifier également le fichier ```CMakeLists.txt``` pour que la génération de message et de descripteur soit prise en compte comme suit

        ```
        find_package(catkin REQUIRED COMPONENTS
        rospy
        sensor_msgs
        std_msgs
        message_generation
        )
        ```
    - Notre package possède maintenant les outils pour compiler les messages et les services custom définis dans le package courant.
    - Pour indiquer où se trouve le descripteur de notre service modifier le fichier ```CMakeLists.txt``` comme suit:

        ```
        add_service_files(
          FILES
          GetImg.srv
        )
        ```
    - Le descripteur de notre service peut utiliser des messages provenant d'autres packages, nous devons indiquer cela dans le fichier ```CMakeLists.txt``` pour que la compilation se passe correctement. Décommenter et modifier la section suivante:
        ```
        generate_messages(
            DEPENDENCIES
            std_msgs sensor_msgs
        )
        ```
    - le fichier ```CMakeLists.txt``` final doit avoir la forme suivante:

        ```yaml
        cmake_minimum_required(VERSION 3.0.2)
        project(img_producer)


        find_package(catkin REQUIRED COMPONENTS
          rospy
          sensor_msgs
          std_msgs
          message_generation
        )

        ## Generate services in the 'srv' folder
         add_service_files(
           FILES
           GetImg.srv
          )


        ## Generate added messages and services with any dependencies listed        here
         generate_messages(
           DEPENDENCIES
           sensor_msgs   std_msgs
         )
        ```
- Compiler votre package
```
catkin_make
```


## 2- Création du contenu du service

- Dans votre package ```img_producer```, créer un répertoire ```scripts```.
```
roscd img_producer
mkdir scripts
```  

- Dans le répertoire  ```scripts```, créer un fichier ```img_producer_node.py``` comme suit: 
```python
#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from sensor_msgs.msg import Image
from PIL import Image as ImagePil
from io import BytesIO
import requests
import numpy
from cv_bridge import CvBridge
import json

class img_producer:
    URL_IMG_RANDOM = "https://source.unsplash.com/random"

    def __init__(self):
        rospy.init_node('img_producer_node', anonymous=False)
        self._bridge = CvBridge()
        rospy.loginfo("[img_producer_node] Started......")
        rospy.spin()

    def generate_img(self, h, w, key_words):
        # Expected format
        # https://source.unsplash.com/1600x900/?nature,water
        url = self.URL_IMG_RANDOM+'/'+str(h) + '*'+ str(w)
        if len(key_words) >0:
           url = url +'/'+'?' 
           for word in key_words:
               url = url + word + ','
        response=requests.get(url)
        pil_image = ImagePil.open(BytesIO(response.content)).convert('RGB') 
        open_cv_image = numpy.array(pil_image)
        return open_cv_image
        

    def convert_img(self, open_cv_image):
        img_msg=self._bridge.cv2_to_imgmsg(open_cv_image, "bgr8")
        return img_msg


if __name__ == '__main__':
    try:
        img_producer()
    except rospy.ROSInterruptException:
        pass

```

- Déclarer un fournisseur de service en modifiant le fichier ```img_producer_node.py``` comme suit: 

```python
...
from img_producer.srv import GetImg, GetImgResponse
...
    def __init__(self):
        ...
        s = rospy.Service('get_img', GetImg, self.get_img_service_callback)

...
    def get_img_service_callback(self,req):
        return GetImgResponse()

```
- Explications:
    ```python
    ...
    import img_producer.srv import GetImg, GetImgResponse
    ...
    ```
    - Permet de récupérer le descripteur de service préalablement créé. ```GetImgResponse``` est un format de message créé lors de la compilation à partir de notre descripteur de service.
    ```python
      ...
        s = rospy.Service('get_img', GetImg, self.get_img_service_callback)
    ```
    - Déclaration d'un fournisseur de service de nom ```get_img```, de type ```GetImg``` qui aura le comportement défini dans la fonction ```get_img_service_callback```

- Compiler votre package

```
catkin_make
```
- Démarrer votre node:

```
source devel.setup.bash
rosrun img_producer img_producer_node.py
```

- Vérifier les services disponibles:
```
rosservice list
```
- Vérifier les informations relatives à votre service
```
rosservice info /get_img
```

- Modifier votre service afin qu'il puisse retourner une image en fontion des arguments choisis:

```python
...
    def get_img_service_callback(self, req):
        open_cv_img= self.generate_img(req.h,req.w,req.theme_list)
        img_msg=self.convert_img(open_cv_img)
        return GetImgResponse(img_msg)
...
```

- Redémarrer votre node

```
rosrun img_producer img_producer_node.py
```
- Tester votre node avec la commande suivante:
```
rosservice call /get_img "{'theme_list':['nature','water'],'h':10,'w':10}"
```

## 3- Création d'un client du service.

- Dans le package ```img_producer```, créer le fichier ```client_img_node.py ``` comme suit:

```python
#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from sensor_msgs.msg import Image
from PIL import Image as ImagePil
from io import BytesIO
import requests
import numpy
import json
import sys
from img_producer.srv import GetImg, GetImgResponse

class client_img:
    URL_IMG_RANDOM = "https://source.unsplash.com/random"

    def __init__(self):
        rospy.init_node('client_img', anonymous=False)
        self.pub = rospy.Publisher('/img_generator/img', Image, queue_size=1)
        rospy.loginfo("[client_img] Started......")

    def process(self,h,w,key_words):
        rospy.wait_for_service('get_img')
        try:
            get_img_service = rospy.ServiceProxy('get_img', GetImg)
            resp1 = get_img_service(key_words,h, w)
            self.pub.publish(resp1.img)
          
        except rospy.ServiceException as e:
            rospy.logwarn("Service call failed: %s"%e)

if __name__ == '__main__':
    h=10
    w=10
    my_list=[]
    if len(sys.argv) >= 4:
        h=int(sys.argv[1])
        w=int(sys.argv[2])
        
        for i in range(3,len(sys.argv)):
            my_list.append(sys.argv[i])
        
        print(my_list)

    try:
        cl_img=client_img()
        cl_img.process(h, w, my_list)
    except rospy.ROSInterruptException:
        pass
```
- Explications 
    ```python
    ...
    def process(self,h,w,key_words):
            rospy.wait_for_service('get_img')
            try:
                get_img_service = rospy.ServiceProxy('get_img', GetImg)
                resp1 = get_img_service(key_words,h, w)
                self.pub.publish(resp1.img)          
            except rospy.ServiceException as e:
                rospy.logwarn("Service call failed: %s"%e)
    ...
    ```
    - ```rospy.wait_for_service('get_img')``` bloque jusqu'a ce que le service soit disponible (possibilité d'ajouter un timeout)   
    - ```get_img_service = rospy.ServiceProxy('get_img', GetImg)``` crée la référence au service
    - ``` resp1 = get_img_service(key_words,h, w) ``` utilise la référence au service en envoyant les arguments et en attendant la réponse (bloquant)

- Tester votre node comme suit:
```
rosrun img_producer img_producer_node.py
rqt_image_view
rosrun img_producer client_img__node.py 100 200 city robot
```









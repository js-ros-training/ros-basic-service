#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from sensor_msgs.msg import Image
from PIL import Image as ImagePil
from io import BytesIO
import requests
import numpy
import json
import sys
from img_producer.srv import GetImg, GetImgResponse

class client_img:
    URL_IMG_RANDOM = "https://source.unsplash.com/random"

    def __init__(self):
        rospy.init_node('client_img', anonymous=False)
        self.pub = rospy.Publisher('/img_generator/img', Image, queue_size=1)
        rospy.loginfo("[client_img] Started......")

    def process(self,h,w,key_words):
        rospy.wait_for_service('get_img')
        try:
            get_img_service = rospy.ServiceProxy('get_img', GetImg)
            resp1 = get_img_service(key_words,h, w)
            self.pub.publish(resp1.img)
          
        except rospy.ServiceException as e:
            rospy.logwarn("Service call failed: %s"%e)

if __name__ == '__main__':
    h=10
    w=10
    my_list=[]
    if len(sys.argv) >= 4:
        h=int(sys.argv[1])
        w=int(sys.argv[2])
        
        for i in range(3,len(sys.argv)):
            my_list.append(sys.argv[i])
        
        print(my_list)

    try:
        cl_img=client_img()
        cl_img.process(h, w, my_list)
    except rospy.ROSInterruptException:
        pass
#!/usr/bin/env python
import rospy
from std_msgs.msg import String
from sensor_msgs.msg import Image
from PIL import Image as ImagePil
from io import BytesIO
import requests
import numpy
from cv_bridge import CvBridge
import json
from img_producer.srv import GetImg, GetImgResponse

class img_producer:
    URL_IMG_RANDOM = "https://source.unsplash.com/random"

    def __init__(self):
        rospy.init_node('img_producer_node', anonymous=False)
        self._bridge = CvBridge()
        self.s = rospy.Service('get_img', GetImg, self.get_img_service_callback)
        rospy.loginfo("[img_producer_node] Started......")
        rospy.spin()

    def generate_img(self, h, w, key_words):
        # Expected format
        # https://source.unsplash.com/1600x900/?nature,water
        url = self.URL_IMG_RANDOM+'/'+str(h) + '*'+ str(w)
        if len(key_words) >0:
           url = url +'/'+'?' 
           for word in key_words:
               url = url + word+','
        response=requests.get(url)
        pil_image = ImagePil.open(BytesIO(response.content)).convert('RGB') 
        open_cv_image = numpy.array(pil_image)
        return open_cv_image
        

    def convert_img(self, open_cv_image):
        img_msg=self._bridge.cv2_to_imgmsg(open_cv_image, "bgr8")
        return img_msg


    def get_img_service_callback(self, req):
        open_cv_img= self.generate_img(req.h,req.w,req.theme_list)
        img_msg=self.convert_img(open_cv_img)
        return GetImgResponse(img_msg)
        
        

if __name__ == '__main__':
    try:
        img_producer()
    except rospy.ROSInterruptException:
        pass